## INTRODUCTION
This modules is for some tweaks to the core link module

At this point this module provides the following tweaks:

You are able to rearrange the order of the title and url field
in the link widget. By default Drupal core link widget puts 
the link field above/before the title field
(Drupal 7 contrib link field module did it the other way round).
This module enables you to change the display order having title
above/before the link (as it was with Drupal 7).
This can be done with a side wide setting for all link fields or
on entity form display base for each link field.

You can add a custom help text on the URI part of the link field
(overwriting the default one, which is not possible via core UI).
Same is possible for the title part of the link field,
which has no help text by default.

You can add a custom help text on the URI part of the link field
(overwriting the default one, which is not possible via core UI).

Provide link field formatter "Link text":
This will allow to output links in link field with fixed text.
Text will be configured as formatter setting.

Mark the Link URI part required if it is required 
(see #2879293: Make Link URI required if there is Link Text input),
sitewide or per field widget setting.

You can choose to extend the labels of autocomplete matches
with entity id and bundle label

## REQUIREMENTS
Drupal core link module

## INSTALLATION
Install module as usual (enable extension)

## CONFIGURATION
configure site wide settings at the module config page
configure widget settings in the form display widget
configure formatter "Link text" in the entity display settings
