<?php

namespace Drupal\link_field_tweak\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;

class EntityAutocompleteController extends \Drupal\system\Controller\EntityAutocompleteController {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('link_field_tweak.entity.autocomplete_matcher'),
      $container->get('keyvalue')->get('entity_autocomplete')
    );
  }

}
