<?php

namespace Drupal\link_field_tweak\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;

trait NodeSelectionIdBundleTrait {

  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if (!$this->getConfiguration()['extended_label'] === TRUE) {
      return parent::getReferenceableEntities($match, $match_operator, $limit);
    }
    $target_type = $this->getConfiguration()['target_type'];

    $query = $this->buildEntityQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }

    $result = $query->execute();

    if (empty($result)) {
      return [];
    }

    $options = [];
    $entities = $this->entityTypeManager->getStorage($target_type)
      ->loadMultiple($result);
    foreach ($entities as $entity_id => $entity) {
      $bundle = $entity->bundle();
      $bundle_label = '';
      if ($entity->getEntityType()->getBundleEntityType()) {
        $bundle_label = $this->entityTypeManager->getStorage($entity->getEntityType()
          ->getBundleEntityType())->load($bundle)->label();
      }
      $options[$bundle][$entity_id] = Html::escape($this->entityRepository->getTranslationFromContext($entity)
        ->label() . ' (- ' . $this->entityRepository->getTranslationFromContext($entity)->id() . ' / ' . $bundle_label . ' -)' ?? '');
    }

    return $options;
  }
}
