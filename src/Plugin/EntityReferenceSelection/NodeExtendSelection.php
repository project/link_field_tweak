<?php

namespace Drupal\link_field_tweak\Plugin\EntityReferenceSelection;

use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;
/**
 * Provides specific access control for the node entity type.
 *
 * @EntityReferenceSelection(
 *   id = "default:nodeextend",
 *   label = @Translation("Node selection extended label"),
 *   entity_types = {"node"},
 *   group = "default",
 *   weight = 1
 * )
 */
class NodeExtendSelection extends NodeSelection {
  use NodeSelectionIdBundleTrait;
}
