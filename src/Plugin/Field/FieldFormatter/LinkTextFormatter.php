<?php

namespace Drupal\link_field_tweak\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link_text' formatter.
 *
 * @FieldFormatter(
 *   id = "link_text",
 *   label = @Translation("Link text"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkTextFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $default_settings = [
        'link_text' => '',
      ] + parent::defaultSettings();
    $default_settings['trim_length'] = '';

    return $default_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['trim_length']['#value'] = '';
    $elements['trim_length']['#disabled'] = TRUE;

    $elements['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $this->getSetting('link_text'),
      '#description' => $this->t('Static link text, will override field link text if any.')
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if (!empty($this->getSetting('link_text'))) {
      $summary[] = $this->t('Link text: @linktext', ['@linktext' => $this->getSetting('link_text')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    $element = parent::viewElements($items, $langcode);

    foreach ($items as $delta => $item) {
      if (!empty($settings['link_text'])) {
        if (!empty($element[$delta]['#title'])) {
          $element[$delta]['#title'] = Xss::filter($settings['link_text']);
        }
      }
    }

    return $element;
  }

}