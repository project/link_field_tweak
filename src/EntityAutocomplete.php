<?php

namespace Drupal\link_field_tweak;

use Drupal\Core\Form\FormStateInterface;

class EntityAutocomplete {
  public static function processEntityAutocomplete(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (isset($element['#autocomplete_route_name_change'])) {
      $element['#autocomplete_route_name'] = $element['#autocomplete_route_name_change'];
    }
    return $element;
  }
}
