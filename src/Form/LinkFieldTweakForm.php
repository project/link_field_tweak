<?php

namespace Drupal\link_field_tweak\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configures link field settings.
 *
 * @package Drupal\link_field_tweak\Form
 */
class LinkFieldTweakForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['link_field_tweak.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'link_field_tweak_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('link_field_tweak.settings');
    $form['widget_field_order'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Switch field order (title field first, link field second)'),
      '#description' => $this->t('This affects every link field widget. You can change this on each link field widget separately if you do not check this box.'),
      '#default_value' => $config->get('widget_field_order'),
    ];
    $form['add_another_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Change text for <em>Add another item</em> to <em>Add another link</em>'),
      '#default_value' => $config->get('add_another_link'),
    ];
    $form['uri_part_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mark the uri part required in front-end when title part is filled'),
      '#description' => $this->t('See <a target="_blank" href="@drupal-issue">Drupal Issue</a> for information about this option. This affects every link field widget. You can change this on each link field widget separately if you do not check this box.',
        [
          '@drupal-issue' => Url::fromUri('https://www.drupal.org/project/drupal/issues/3203656')->toString(),
        ]
      ),
      '#default_value' => $config->get('uri_part_required'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('link_field_tweak.settings')
      ->set('widget_field_order', $form_state->getValue('widget_field_order'))
      ->set('add_another_link', $form_state->getValue('add_another_link'))
      ->set('uri_part_required', $form_state->getValue('uri_part_required'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
